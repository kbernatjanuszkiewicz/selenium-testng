package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MainTestTest {
        WebDriver driver;


        @BeforeClass(alwaysRun = true)
        public void setup() {
            System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Webdrivers\\chromedriver.exe");
            this.driver = new ChromeDriver();
        }

        @AfterClass(alwaysRun = true)
        public void tearDown() {
            this.driver.quit();
        }


    @Test(dataProvider="pages")
    public void testMethod(String url, String title) throws InterruptedException{
        {
            driver.get(url);
            Assert.assertEquals(driver.getTitle(), title);
        }
    }

    @DataProvider(name="pages")
    public Object[][] getDataFromDataprovider(){
        return new Object[][]
                {
                        { "https://www.facebook.com", "Facebook – zaloguj się lub zarejestruj"},
                        { "https://www.google.com", "Google"},
                        { "https://www.yahoo.com", "Yahoo jest teraz częścią rodziny Oath" }
                };
    }
}

