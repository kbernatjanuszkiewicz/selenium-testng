package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.*;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Main {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Webdrivers\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://demo.guru99.com/test/yahoo.html");

        WebElement downlaodButton = driver.findElement(By.id("messenger-download"));
        String sourceLocation = downlaodButton.getAttribute("href");
        String wget_command = "cmd /c C:\\Users\\kbernatjanuszkiewicz\\Desktop\\wget.exe -P D: --no-check-certificate " + sourceLocation;

        try {
            Process exec = Runtime.getRuntime().exec(wget_command);
            int exitVal = exec.waitFor();
            System.out.println("Exit value: " + exitVal);
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        driver.close();

    }
}
